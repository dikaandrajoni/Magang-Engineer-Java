package com.dika.crud_organisasi.controller;

import com.dika.crud_organisasi.model.Company;
import com.dika.crud_organisasi.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @RequestMapping(value = "/company")
    public String listCompany(Model model){
        List<Company> listAllCompany = companyService.listAllCompany();
        model.addAttribute("listAllCompany", listAllCompany);
        return "company/list_company";
    }
    @RequestMapping(value = "/company/new")
    public String addCompany(Model model){
        Company company = new Company();
        model.addAttribute("company", company);
        return "company/tambahCompany";
    }

    @RequestMapping(value = "/company/save", method = RequestMethod.POST)
    public String saveCompany(@ModelAttribute("company") Company company){
        companyService.save(company);
        return "redirect:/company";
    }

    @RequestMapping(value = "/company/edit/{id}")
    public ModelAndView editCompany(@PathVariable(name = "id") int id){
        ModelAndView mv = new ModelAndView("company/editCompany");
        Company company = companyService.getCompany(id);
        mv.addObject("company", company);
        return mv;
    }

    @RequestMapping("/company/delete/{id}")
    public String deleteCompany(@PathVariable(name = "id") int id) {
        companyService.delete(id);
        return "redirect:/company";
    }

}

package com.dika.crud_organisasi.repository;

import com.dika.crud_organisasi.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query("SELECT e.id, e.nama, if(em.nama=e.nama,null, em.nama) as atasan, c.nama from employee e join employee em join company c on e.company_id = c.id where e.atasan_id = em.id or e.atasan_id is null group by e.id;")
		public findAll(){
		}
}

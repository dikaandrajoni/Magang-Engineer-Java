package com.dika.crud_organisasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudOrganisasiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudOrganisasiApplication.class, args);
	}

}

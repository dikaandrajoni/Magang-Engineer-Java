package com.dika.crud_organisasi.controller;

import com.dika.crud_organisasi.model.Employee;
import com.dika.crud_organisasi.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@Controller
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/employee")
    public String listEmployee(Model model){
        List<Employee> listEmployee = employeeService.listEmployee();
        model.addAttribute("listEmployee", listEmployee);
        return "employee/list_employee";
    }
    @RequestMapping(value = "/employee/new")
    public String addEmployee(Model model){
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "employee/tambahEmployee";
    }

    @RequestMapping(value = "/employee/save", method = RequestMethod.POST)
    public String saveEmployee(@ModelAttribute("employee") Employee employee){
        employeeService.save(employee);
        return "redirect:/employee";
    }

    @RequestMapping(value = "/employee/edit/{id}")
    public ModelAndView editEmployee(@PathVariable(name = "id") int id){
        ModelAndView mv = new ModelAndView("employee/editEmployee");
        Employee employee = employeeService.getEmployee(id);
        mv.addObject("employee", employee);
        return mv;
    }

    @RequestMapping("/employee/delete/{id}")
    public String deleteEmployee(@PathVariable(name = "id") int id) {
        employeeService.delete(id);
        return "redirect:/employee";
    }



}

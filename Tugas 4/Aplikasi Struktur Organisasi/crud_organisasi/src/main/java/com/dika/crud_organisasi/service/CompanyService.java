package com.dika.crud_organisasi.service;

import com.dika.crud_organisasi.model.Company;
import com.dika.crud_organisasi.model.Employee;
import com.dika.crud_organisasi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CompanyService {
    @Autowired
    CompanyRepository repo;

    public List<Company> listAllCompany(){
        return repo.findAll();
    }

    public void save(Company company){
        repo.save(company);
    }

    public Company getCompany(int id){
        return repo.findById(id).get();
    }

    public void delete(int id){
        repo.deleteById(id);
    }
}

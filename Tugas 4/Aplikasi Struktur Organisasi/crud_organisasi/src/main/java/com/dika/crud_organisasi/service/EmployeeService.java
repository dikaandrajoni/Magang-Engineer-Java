package com.dika.crud_organisasi.service;

import com.dika.crud_organisasi.model.Employee;
import com.dika.crud_organisasi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> listEmployee(){
        return employeeRepository.findAll();
    }

    public void save(Employee employee){
        employeeRepository.save(employee);
    }

    public Employee getEmployee(int id){
        return employeeRepository.findById(id).get();
    }

    public void delete(int id){
        employeeRepository.deleteById(id);
    }
}

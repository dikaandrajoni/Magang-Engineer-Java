package com.dika.crud_organisasi.repository;

import com.dika.crud_organisasi.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Integer> {
}

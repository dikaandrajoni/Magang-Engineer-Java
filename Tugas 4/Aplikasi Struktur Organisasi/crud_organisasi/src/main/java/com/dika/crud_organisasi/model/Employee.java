package com.dika.crud_organisasi.model;

import javax.persistence.*;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "nama")
    private String nama;
    @Column(name = "atasan_id")
    private int atasan_id;
    @Column(name = "company_id")
    private int company_id;

    public Employee() {
    }

    public Employee(int id, String nama, int atasan_id, int company_id) {
        this.id = id;
        this.nama = nama;
        this.atasan_id = atasan_id;
        this.company_id = company_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getAtasan_id() {
        return atasan_id;
    }

    public void setAtasan_id(int atasan_id) {
        this.atasan_id = atasan_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas1_Basic_Programming;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author USER DSC
 */
public class HitungVokal {
    
    
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        String data;
        int ulang=0;
        do{
            System.out.print("Input : ");
            data = in.nextLine();
            CekVokal(data);
            ulang++;
        }
        while(ulang<3);
        
    }
    
    public static void CekVokal(String str){
        int a=0,i=0,u=0,e=0,o=0;
        char ch = 0;
        String tampung="";
        String[] huruf = null; 
        str=str.toLowerCase();
        huruf = str.split(" ");
        for (int x = 0; x < huruf.length; x++) {
            for (int y = 0; y < huruf[x].length(); y++) {
                ch = huruf[x].charAt(y);
                
                if (ch == 'a'){
                    a=1;
                }else if(ch == 'i' ){
                    i=1;
                }else if(ch == 'u' ){
                    u=1;
                } else if(ch == 'e'){
                    e=1;
                }else if(ch == 'o'){
                    o=1;
                }
                
            }
            int jmlVokal=(a+i+u+e+o);
            if(a==1){
                tampung+="a ";
            }
            if(i==1){
                tampung+="i ";
            }
            if(u==1){
                tampung+="u ";
            }
            if(e==1){
                tampung+="e ";
            }
            if(o==1){
                tampung+="o ";
            }
            System.out.println(str +" = "+jmlVokal+" yaitu "+tampung);
            System.out.println();
        }
       
    }
}

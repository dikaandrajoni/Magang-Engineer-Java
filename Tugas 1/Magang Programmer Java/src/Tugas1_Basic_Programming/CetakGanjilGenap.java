/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas1_Basic_Programming;

import java.util.Scanner;

/**
 *
 * @author USER DSC
 */
public class CetakGanjilGenap {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int A,B;
        System.out.print("Input A: ");
        A = input.nextInt();
        System.out.print("Input B: ");
        B = input.nextInt();
        System.out.println();
        
        CekAngka(A,B);
        
        
    }
    
    public static void CekAngka(int a, int b){
        for(int i=a; i<=b; i++){
            if(i%2==0){
                System.out.println("Angka " +i+" adalah Genap");
            } else{
                System.out.println("Angka " +i+" adalah Ganjil");
            }
        
        }
        
    }
}

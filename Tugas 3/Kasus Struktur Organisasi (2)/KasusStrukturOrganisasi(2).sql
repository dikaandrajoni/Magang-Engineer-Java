
insert into company values ('1', 'PT JAVAN','Sleman'),
                           ('2', 'PT Dicoding', 'Bandung');

insert into employee values ('1', 'Pak Budi',null, '1'),
                           ('2', 'Pak Tono', '1','1'),
                           ('3', 'Pak Totok', '1','1'),
                           ('4', 'Bu Sinta', '2','1'),
                           ('5', 'Bu Novi', '3','1'),
                           ('6', 'Andre', '4','1'),
                           ('7', 'Dono', '4','1'),
                           ('8', 'Ismir', '5','1'),
                           ('9', 'Anto', '5','1');

SELECT nama FROM employee where atasan_id is null;

SELECT nama FROM employee where atasan_id in (SELECT id FROM employee where atasan_id in (SELECT id FROM employee where atasan_id = (SELECT id FROM employee where atasan_id is null)));

SELECT nama FROM employee where atasan_id in (SELECT id FROM employee where atasan_id is null);

SELECT nama FROM employee where atasan_id in (SELECT id FROM employee where atasan_id = (SELECT id FROM employee where atasan_id is null));

select count(*) as jumlah_bawahan from employee where nama not in (?); #Input nama dengan tanda "" Digunakan untuk menampilkan bawahan dari pak budi

select @atasanid := id from employee where nama = ?;   # Ini query dilakukan dengan 3 tahap
select case
 when @atasanid=2 then @atasan := id
    when @atasanid=3 then @atasan := id
    when @atasanid=4 then @atasan := id
    when @atasanid=5 then @atasan := id end
from employee where  atasan_id=@atasanid;
select count(id) as jumlah_bawahan from employee where atasan_id=@atasan;

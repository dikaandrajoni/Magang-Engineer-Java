package com.example.ganjilgenapmvc.model;


import java.util.List;

public class ModelGanjilGenap {
    private int awal;
    private int akhir;
    private String[] hasil;

    public ModelGanjilGenap() {
    }

    public ModelGanjilGenap(int awal, int akhir) {
        this.awal = awal;
        this.akhir = akhir;
    }

    public String[] getHasil() {
        return hasil;
    }

    public void setHasil(String[] hasil) {
        this.hasil = hasil;
    }

    public int getAwal() {
        return awal;
    }

    public void setAwal(int awal) {
        this.awal = awal;
    }

    public int getAkhir() {
        return akhir;
    }

    public void setAkhir(int akhir) {
        this.akhir = akhir;
    }

    public String[] Cek(){
        String[] hasil = new String[(akhir-awal+1)];
        String temp;
        int a=0;
        if(akhir>awal){
            for(int i=this.awal; i<=this.akhir; i++){
                    if(i%2==0){
                        temp="Genap";
                    }else {
                        temp="Ganjil";
                    }
                    hasil[a] = i+" Angka "+temp;
                    a++;
                }
        }else{
            hasil[a]="Angka 2 Lebih kecil dari angka 1";
            a++;
        }


        return hasil;
    }
}

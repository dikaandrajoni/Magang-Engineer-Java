package com.example.ganjilgenapmvc.controller;

import com.example.ganjilgenapmvc.model.ModelGanjilGenap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ControllerGanjilGenap {
    ModelGanjilGenap ganjilGenap = new ModelGanjilGenap();

    @RequestMapping("/")
    public String cekGanjilGenap(Model model){
        model.addAttribute("ganjilGenap", ganjilGenap);
        return "index";
    }

    @RequestMapping(value = "/", params = "cek", method = RequestMethod.POST)
    public String tambah(@ModelAttribute("ganjilGenap") ModelGanjilGenap ganjilGenap, Model model){
        model.addAttribute("hasils", ganjilGenap.Cek());
        return "index";

    }



}

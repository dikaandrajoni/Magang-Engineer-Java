package com.example.ganjilgenapmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.example.ganjilgenapmvc.controller"})
public class GanjilgenapmvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(GanjilgenapmvcApplication.class, args);
	}

}

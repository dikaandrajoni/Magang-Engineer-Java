package com.hitungvokalmvc.hitungvokalmvc.model;

import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.regex.Pattern;

public class ModelHitungVokal {
    @NotNull
    @SafeHtml
    private String data;

    public ModelHitungVokal() {

    }

    public ModelHitungVokal(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String[] cekVokal() {
        int x = 0;
        boolean aa = false, ii = false, uu = false, ee = false, oo = false;
        String kalimat = this.data;
        String[] str = new String[5];
        String[] hasil = new String[3];
        StringBuilder out = null;


            for (int i = 0; i < kalimat.length(); i++) {
                if(kalimat.charAt(i)=='<' || kalimat.charAt(i)== '/' || kalimat.charAt(i)=='?'){
                    kalimat = kalimat.replaceAll("<", " ");
                    kalimat = kalimat.replaceAll(">", " ");
                    kalimat = kalimat.replaceAll("/", " ");

                }
            }

            for (int i = 0; i < kalimat.length(); i++) {
                if (!aa) {
                    if (kalimat.charAt(i) == 'a' || kalimat.charAt(i) == 'A') {
                        str[x] = "a";
                        x++;
                        aa = true;
                    }
                }
                if (!ii) {
                    if (kalimat.charAt(i) == 'i' || kalimat.charAt(i) == 'I') {
                        str[x] = "i";
                        x++;
                        ii = true;
                    }
                }
                if (!uu) {
                    if (kalimat.charAt(i) == 'u' || kalimat.charAt(i) == 'U') {
                        str[x] = "u";
                        x++;
                        uu = true;
                    }
                }
                if (!ee) {
                    if (kalimat.charAt(i) == 'e' || kalimat.charAt(i) == 'E') {
                        str[x] = "e";
                        x++;
                        ee = true;
                    }
                }
                if (!oo) {
                    if (kalimat.charAt(i) == 'o' || kalimat.charAt(i) == 'O') {
                        str[x] = "o";
                        x++;
                        oo = true;
                    }
                }
            }
            for (int i = 0; i < str.length; i++) {
                if (str[i] != null) {
                    if (i == 0) {
                        out = new StringBuilder(str[i]);
                    } else {
                        Objects.requireNonNull(out).append(" dan ").append(str[i]);
                    }
                }
            }
            hasil[0] = '"' + kalimat + '"' + " = " + x + " yaitu " + Objects.requireNonNull(out).toString();

        return hasil;
    }



}

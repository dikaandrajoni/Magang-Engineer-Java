package com.hitungvokalmvc.hitungvokalmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HitungvokalmvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(HitungvokalmvcApplication.class, args);
	}

}

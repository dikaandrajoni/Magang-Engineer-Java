package com.hitungvokalmvc.hitungvokalmvc.controller;

import com.hitungvokalmvc.hitungvokalmvc.model.ModelHitungVokal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class ControllerHitungVokal {
    ModelHitungVokal mdl = new ModelHitungVokal();

    @RequestMapping("/")
    public String hitungVokal(Model model){
        model.addAttribute("mdl", mdl);
        return "index";
    }

    @RequestMapping(value = "/", params = "cek", method = RequestMethod.POST)
    public String cari(@ModelAttribute("mdl") ModelHitungVokal mdl, Model model){
        model.addAttribute("hasil", mdl.cekVokal());
        return "index";
    }

}
